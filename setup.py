from setuptools import setup

setup(name='saw2_analysis',
      version='0.1',
      description='Tools to analyse SaWLens2 output',
      url='https://bitbucket.org/jmerten82/saw2_analysis',
      author='Julian Merten',
      author_email='julian.merten@physics.ox.ac.uk',
      license='Oxford',
      packages=['saw2_analysis','saw2_analysis.analysis','saw2_analysis.widefield','saw2_analysis.io','saw2_analysis.visualisation'],
      zip_safe=False)
