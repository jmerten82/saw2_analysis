import numpy
import matplotlib.pyplot as plt
import pyfits
import h5py
from astropy.table import Table

#Quick little helper routine, building a unit grid from given pixel size

def build_unit_grid(num,spare_centre=False):
    output=[]
    for y in numpy.linspace(-1.,1.,num):
        for x in numpy.linspace(-1.,1.,num):
            if(spare_centre):
                if(abs(x) > .5 or abs(y) > .5):
                    output.append((x,y))
            else:
                output.append((x,y))
    return numpy.array(output)
                
    



#This class implements a simple repesensation of a filler node structure for to be used within SaWLens2. It can create regular and random meshes within a certain coordinate range and create  reulariation schemes on that structure. 

class FillerNodeMap(object):

    #Standard constructor, either creating a random or regular the map on a unit coordinate system or readging coordinates from ASCII. 

    def __init__(self, nodes=0, type="random", refinement_level=0):

        if(nodes != 0):
            if(type == "random"):
                radii = []
                x_array=[]
                y_array=[]
                radii.append(1.)
                for i in range(1,refinement_level+1):
                    radii.append(0.5**i)
                radii=numpy.array(radii)

                areas = []
                density = 0.
                for i in range(0,refinement_level+1):
                    current_area = (i+1.)*numpy.pi*radii[i]**2
                    areas.append(current_area)
                    density += current_area
                areas = numpy.array(areas)
                density = nodes/density
                    

                for i in range(0,refinement_level+1):
                    areas[i] *= density
                    for j in numpy.arange(0,areas[i]):
                        radius = numpy.sqrt(numpy.random.rand()*radii[i]**2) 
                        phi = numpy.random.rand()*2.*numpy.pi
                        x_array.append(radius*numpy.cos(phi))
                        y_array.append(radius*numpy.sin(phi))
                        
                self.coords=numpy.array(zip(x_array,y_array))


            else:
                nodes_per_refinement = 0.
                for i in range(0,refinement_level+1):
                    if(i==refinement_level):
                        nodes_per_refinement += 1.
                    else:
                        nodes_per_refinement += .75
                nodes_per_refinement = nodes / nodes_per_refinement
                self.coords = numpy.zeros([0,2])
                for i in range(0,refinement_level+1):
                    if(i==refinement_level):
                        current_coords = build_unit_grid(int(numpy.sqrt((numpy.ceil(nodes_per_refinement)))))
                    else:
                        current_coords = build_unit_grid(int(numpy.sqrt((numpy.ceil(nodes_per_refinement)))),True)
                    current_coords *= 0.5**i
                    self.coords = numpy.vstack([self.coords,current_coords])
        else:
            self.coords=numpy.zeros([0,2])


        self.c_reg = numpy.zeros_like(self.coords)
        self.s1_reg = numpy.zeros_like(self.coords)
        self.s2_reg = numpy.zeros_like(self.coords)
                

    #This creates a simple matplotlib instance, visualising the grid coordinate distribution.
    def show(self):
        self.fig, self.ax = plt.subplots()
        my_ax = self.ax.plot(self.coords[:,0],self.coords[:,1],'ko')


    #This lets you read coordinates from a simple ASCII reading. The first two columns in the ascii file are expected to be the x and y coordinates

    def read(self, file):
        try:
            data = numpy.genfromtxt(file,comments='#')
            self.coords = numpy.array(zip(data[:,0],data[:,1]))
            self.c_reg = numpy.zeros_like(self.coords)
            self.s1_reg = numpy.zeros_like(self.coords)
            self.s2_reg = numpy.zeros_like(self.coords)
        except:
            print "FillerNodeMap read: Something is wrong with your file."

    # This lets you shift the x and y coordinates by the value specified.

    def shift(self, x=0.,y=0.):
        self.coords[:,0] += x
        self.coords[:,1] += y

    # This lets you scale all coordinates by a given factor.
    def scale(self, scaling=1.):
        self.coords *= scaling

    #This writes the class in standard SaWLens 2 filler node format. Format selection are ascii, fits and hdf5 
    def output_saw2_format(self,filename='./filler_grid.txt',format='ascii'):
        
        if(format=="fits"):
            x_col = pyfits.Column(name='x',format='D',array=self.coords[:,0])
            y_col = pyfits.Column(name='y',format='D',array=self.coords[:,1])
            ct_col = pyfits.Column(name='c_templ',format='D',array=self.c_reg[:,0]) 
            cp_col = pyfits.Column(name='c_param',format='D',array=self.c_reg[:,1]) 
            s1t_col = pyfits.Column(name='s1_templ',format='D',array=self.s1_reg[:,0]) 
            s1p_col = pyfits.Column(name='s1_param',format='D',array=self.s1_reg[:,1])
            s2t_col = pyfits.Column(name='s2_templ',format='D',array=self.s2_reg[:,0]) 
            s2p_col = pyfits.Column(name='s2_param',format='D',array=self.s2_reg[:,1])
            tbhdu = pyfits.new_table([x_col,y_col,ct_col,cp_col,s1t_col,s1p_col,s2t_col,s2p_col])
            tbhdu.writeto(filename,clobber=True)

        elif(format=="hdf5"):
            mytable = Table([self.coords[:,0],self.coords[:,1],self.c_reg[:,0],self.c_reg[:,1],self.s1_reg[:,0],self.s1_reg[:,1],self.s2_reg[:,0],self.s2_reg[:,1]],names=('x','y','c_templ','c_param','s1_templ','s1_param','s2_templ','s2_param'),dtype=('f8','f8','f8','f8','f8','f8','f8','f8'))
            mytable.write(filename,format='hdf5',path='catalogue')
        else:
            output = open(filename,'w')
            print output, "# x y c_templ c_param s1_templ s1_param s2_templ s2_param"
            for i in range(0,len(coords)):
                print output, self.coords[i][0], self.coords[i][1], self.c_reg[i][0], self.c_reg[i][1], self.s1_reg[i][0], self.s1_reg[i][1], self.s2_reg[i][0], self.s2_reg[i][1]
            output.close()
            
        


    #This returns the full content of the class as an astropy Table.
    def table(self):
        mytable = Table([self.coords[:,0],self.coords[:,1],self.c_reg[:,0],self.c_reg[:,1],self.s1_reg[:,0],self.s1_reg[:,1],self.s2_reg[:,0],self.s2_reg[:,1]],names=('x','y','c_templ','c_param','s1_templ','s1_param','s2_templ','s2_param'),dtype=('f8','f8','f8','f8','f8','f8','f8','f8'))
        return mytable

    def set_reg(self,selection="all",function=None, value=0.,strength=1.):
        if(function==None):
            if(selection=="convergence"):
                self.c_reg[:,0] = value
                self.c_reg[:,1] = strength
            elif(selection == "shear1"):
                self.s1_reg[:,0] = value
                self.s1_reg[:,1] = strength
            elif(selection == "shear2"):
                self.s2_reg[:,0] = value
                self.s2_reg[:,1] = strength
            else:
                self.c_reg[:,0] = value
                self.c_reg[:,1] = strength
                self.s1_reg[:,0] = value
                self.s1_reg[:,1] = strength
                self.s2_reg[:,0] = value
                self.s2_reg[:,1] = strength

        else:
            value = []
            strenghts = numpy.zeros(len(self.coords))
            
            for thisone in self.coords:
                value.append(function(thisone[0],thisone[1]))
            value = numpy.array(value)
            if(selection == "convergence"):
                self.c_reg=numpy.array(zip(value,strenghts))
            elif(selection == "shear1"):
                self.s1_reg=numpy.array(zip(value,strenghts))
            elif(selection == "shear2"):
                self.s2_reg=numpy.array(zip(value,strenghts))
            else:
                self.c_reg=numpy.array(zip(value,strenghts))
                self.s1_reg=numpy.array(zip(value,strenghts))
                self.s2_reg=numpy.array(zip(value,strenghts))

    def set_strength(self,selection='all',strength=1.):
        if(selection == "convergence"):
            self.c_reg[:,1]=strength
        elif(selection == "shear1"):
            self.s1_reg[:,1]=strength
        elif(selection == "shear2"):
            self.s2_reg[:,1]=strength
        else:
            self.c_reg[:,1]=strength
            self.s1_reg[:,1]=strength
            self.s2_reg[:,1]=strength
