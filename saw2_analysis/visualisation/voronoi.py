import scipy
import numpy
import pyfits
from scipy import spatial
import matplotlib
import matplotlib.pyplot as plt


# This central routine in this collection takes a given SaW2 map and uses a Voronoi cell scheme to create an oversampled, regular mxn output matrix. You can choose the number of pixels on the x axis, together with an output window. If the output window is empty, the min and max coordinate values on each axes are calculated first. If the mask threshold is set to something postive, the routine checks if the distance of each oversampled pixel to its nearest real number is larger than the threshold. If so, the pixel is set to the mask_value value.  

def voronoi_map(map,x_sampling=1024,x_min=None,x_max=None,y_min=None,y_max=None, mask_threshold = -1, mask_value = 0.):

    if(x_min==x_max):
        x_min = numpy.min(map[:,0])
        x_max = numpy.max(map[:,0])

    if(y_min==y_max):
        y_min = numpy.min(map[:,1])
        y_max = numpy.max(map[:,1])

    x_range = (x_max-x_min)
    step = x_range/x_sampling
    y_range = (y_max-y_min)
    ratio = y_range/x_range
    y_sampling = int(ratio*x_sampling)

    masked = False
    if (mask_threshold > 0.):
        masked = True

    coords = map[:,0:2]
    Tree=scipy.spatial.cKDTree(coords,leafsize=128)
    over_sample = numpy.zeros([y_sampling,x_sampling])
    for i in range(0,y_sampling):
        for j in range(0,x_sampling):
            x_coord = x_min + j*step
            y_coord = y_min + i*step
            index = Tree.query((x_coord,y_coord),1)
            if(masked and index[0] > mask_threshold):
                over_sample[i][j] = mask_value
            else:
                over_sample[i][j] = map[index[1]][2]


    return over_sample

#This routine creates a fits file from a coordinate-value map by oversampling and via a Voronoi approach. The coorindate ranges and the number of pixels in the x-direction have to be provided. See above for masking explanation

def voronoi_fits_from_map(filename, map, x1, x2,x_dim=512,y1=0., y2=0., mask_threshold = -1, mask_value = 0.):

    if(y1 == y2):
        y1 = x1
        y2 = y2
    over_sample = voronoi_map(map,x_dim,x1,x2,y1,y2,mask_threshold,mask_value)
    step = (x2-x1)/x_dim

    hdu = pyfits.PrimaryHDU(over_sample)
    current_header = hdu.header
    hdu.header.append(('CRVAL1', x1, ''))
    hdu.header.append(('CRVAL2', y1, ''))
    hdu.header.append(('CRPIX1', 1.0, ''))
    hdu.header.append(('CRPIX2', 1.0, ''))
    hdu.header.append(('CD1_1', step, ''))
    hdu.header.append(('CD2_2', step, ''))
    hdu.header.append(('CD1_2', 0., ''))
    hdu.header.append(('CD2_1', 0., ''))
    hdu.header.append(('CTYPE1',' ' , ''))
    hdu.header.append(('CTYPE2',' ' , ''))
    hdulist = pyfits.HDUList([hdu])
    hdulist.writeto(filename,clobber=True)


#This takaes an existing single map and visualises it as an matplotlib imshow output. 

#def show_maps():

