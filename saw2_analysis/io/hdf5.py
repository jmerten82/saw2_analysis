import numpy
import h5py

# The following extracts the kappa reconstruction from a valid SaWLens2 hdf5 output file. It returns the x/y coordinates and the kappa value for each final output node as an array of shape (num_nodes,3).


def get_kappa_data_from_saw2_hdf5(hdf5_file_object):
    
    data_out=[]
    coordinates=hdf5_file_object['reconstruction/coordinates']
    kappa=hdf5_file_object['reconstruction/reconstruction']['kappa']
    data_out=numpy.zeros([len(coordinates),3])
    scale = hdf5_file_object.attrs['Coordinate scale']
    shift1 = hdf5_file_object.attrs['Coordinate x-shift']
    shift2 = hdf5_file_object.attrs['Coordinate y-shift']
    for index in range(0,len(coordinates)):
        data_out[index][0]=(coordinates[index][0] + shift1) * scale 
        data_out[index][1]=(coordinates[index][1] + shift2) * scale
        data_out[index][2]=kappa[index]

    return data_out

def get_map_from_saw2_hdf5(hdf5_file_object,selection = "kappa"):
    
    data_out=[]
    coordinates=hdf5_file_object['reconstruction/coordinates']
    if(selection == "jacdet"):
        map1 = hdf5_file_object['reconstruction/reconstruction']['kappa']
        map2 = hdf5_file_object['reconstruction/reconstruction']['gamma1']
        map3 = hdf5_file_object['reconstruction/reconstruction']['gamma2']
        map = (1.-map1)**2. - map2**2. - map3**2.

    elif(selection == "magnification"):
        map1 = hdf5_file_object['reconstruction/reconstruction']['kappa']
        map2 = hdf5_file_object['reconstruction/reconstruction']['gamma1']
        map3 = hdf5_file_object['reconstruction/reconstruction']['gamma2']
        map = 1./((1.-map1)**2. - map2**2. - map3**2.)

    else:
        map=hdf5_file_object['reconstruction/reconstruction'][selection]

    data_out=numpy.zeros([len(coordinates),3])
    scale = hdf5_file_object.attrs['Coordinate scale']
    shift1 = hdf5_file_object.attrs['Coordinate x-shift']
    shift2 = hdf5_file_object.attrs['Coordinate y-shift']
    for index in range(0,len(coordinates)):
        data_out[index][0]=(coordinates[index][0] + shift1) * scale 
        data_out[index][1]=(coordinates[index][1] + shift2) * scale
        data_out[index][2]=map[index]

    return data_out


# This routine returns all reconstruction quantities from a SaW2 hdf5 file. The return is an array of shape (numnodes,8) and follows the ordering in the hdf5 file. 


def get_all_data_from_saw2_hdf5(hdf5_file):  

    data_out=[]
    coordinates=hdf5_file['/reconstruction/coordinates']
    psi=hdf5_file['/reconstruction/reconstruction']['psi']
    alpha1=hdf5_file['/reconstruction/reconstruction']['alpha1']
    alpha2=hdf5_file['/reconstruction/reconstruction']['alpha2']
    kappa=hdf5_file['/reconstruction/reconstruction']['kappa']
    gamma1=hdf5_file['/reconstruction/reconstruction']['gamma1']
    gamma2=hdf5_file['/reconstruction/reconstruction']['gamma2']
    data_out=numpy.zeros([len(coordinates),8])
    scale = hdf5_file.attrs['Coordinate scale']
    shift1 = hdf5_file.attrs['Coordinate x-shift']
    shift2 = hdf5_file.attrs['Coordinate y-shift']
    for index in range(0,len(coordinates)):
        data_out[index][0]=(coordinates[index][0] + shift1) * scale 
        data_out[index][1]=(coordinates[index][1] + shift2) * scale
        data_out[index][2]=psi[index]
        data_out[index][3]=alpha1[index]
        data_out[index][4]=alpha2[index]
        data_out[index][5]=kappa[index]
        data_out[index][6]=gamma1[index]
        data_out[index][7]=gamma2[index]

    return data_out

#The following reads all BS realisations from a given H5 path into an array. Each object in the array will contain the coordinates and the map itself.

def read_BS_into_array(H5_path, selection="kappa", min = 0., max = 0., max_num = -1):

    out_array = []

    if(max_num < 0):
        for object in H5_path["/BS/"]:

            coordinates = H5_path["/BS/"+str(object)+"/reconstruction/coordinates"]
            if(selection == "jacdet"):
                map1 = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"]["kappa"]
                map2 = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"]["gamma1"]
                map3 = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"]["gamma2"]
                map = (1.-map1)**2. - map2**2. - map3**2. 

            elif(selection == "magnification"):
                map1 = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"]["kappa"]
                map2 = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"]["gamma1"]
                map3 = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"]["gamma2"]
                map = 1./((1.-map1)**2. - map2**2. - map3**2.) 

            else:

                map = H5_path["/BS/"+str(object)+"/reconstruction/reconstruction"][selection]


            data_out=numpy.zeros([len(coordinates),3])
            scale = H5_path["/BS/"+str(object)].attrs['Coordinate scale']
            shift1 = H5_path["/BS/"+str(object)].attrs['Coordinate x-shift']
            shift2 = H5_path["/BS/"+str(object)].attrs['Coordinate y-shift']
            for index in range(0,len(coordinates)):
                data_out[index][0]=(coordinates[index][0] + shift1) * scale 
                data_out[index][1]=(coordinates[index][1] + shift2) * scale
                data_out[index][2]=map[index]

            if(min != max):
                if(numpy.min(numpy.transpose(data_out)[2]) >= min and numpy.max(numpy.transpose(data_out)[2]) <= max):
                    out_array.append(data_out)

            else:
                out_array.append(data_out)

    else:
        for index in range(0,max_num):
            coordinates = H5_path["/BS/"+str(index)+"/reconstruction/coordinates"]   

            if(selection == "jacdet"):
                map1 = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"]["kappa"]
                map2 = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"]["gamma1"]
                map3 = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"]["gamma2"]
                map = (1.-map1)**2. - map2**2. - map3**2. 

            elif(selection == "magnification"):
                map1 = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"]["kappa"]
                map2 = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"]["gamma1"]
                map3 = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"]["gamma2"]
                map = 1./((1.-map1)**2. - map2**2. - map3**2.) 

            else:

                map = H5_path["/BS/"+str(index)+"/reconstruction/reconstruction"][selection]


            data_out=numpy.zeros([len(coordinates),3])
            scale = H5_path["/BS/"+str(max_num)].attrs['Coordinate scale']
            shift1 = H5_path["/BS/"+str(max_num)].attrs['Coordinate x-shift']
            shift2 = H5_path["/BS/"+str(max_num)].attrs['Coordinate y-shift']
            for index in range(0,len(coordinates)):
                data_out[index][0]=(coordinates[index][0] + shift1) * scale 
                data_out[index][1]=(coordinates[index][1] + shift2) * scale
                data_out[index][2]=map[index]
            
            if(min != max):
                if(numpy.min(numpy.transpose(data_out)[2]) >= min and numpy.max(numpy.transpose(data_out)[2]) <= max):
                    out_array.append(data_out)

            else:
                out_array.append(data_out)

    out_array = numpy.array(out_array)
    
    return out_array


