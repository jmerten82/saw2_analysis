import numpy 
import pyfits

# The following takes a FITS file object, the sidelength of the x-axis, the pixel coordinates of a reference point and return the coordinates and map value as an objects with shape (num_pixels, 3). The index of the fits HDU can be provided

def get_map_from_fits(fits_file_object, extension = 0, sidelength = 1., x_ref = 0, y_ref = 0):
    map = fits_file_object[extension].data
    x_dim = numpy.shape(map)[0]
    y_dim = numpy.shape(map)[1]
    pixel_size = sidelength/x_dim

    data_output = []

    for y in range(0,y_dim):
        for x in range(0,x_dim):
            current_x = (x-x_ref)*pixel_size
            current_y = (y-y_ref)*pixel_size
            current_value = map[y][x]
            data_output.append((current_x,current_y,current_value))
        
    data_output = numpy.array(data_output)

    return data_output
    
    
