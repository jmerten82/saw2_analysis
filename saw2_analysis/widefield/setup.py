import numpy

#This routine creates standard SaWLens2 configureation files after reading an initial file, which must contain the full field size in the 'catalogue_window' field. You must also provide the number of output tiles in x and in y direction, and the size of the overlap that the tiles should have. The output are x*y output configuration files which will be save in $(output_mask)_y_x.conf.

def create_standard_configs(input_config, output_mask="./tile",  num_x=5, num_y=5, overlap_x=0.,overlap_y=-1.):
    
    x0 = 0.
    x1 = 0.
    y0 = 0.
    y1 = 0.

    if(overlap_y == -1.):
        overlap_y = overlap_x

    initial_config = open(input_config,'r')
    for line in initial_config:
        if(line.find("catalogue_window")!=-1):
            line = line.replace("catalogue_window","").lstrip()
            line_array = line.split(",")
            x0 = float(line_array[0].lstrip().rstrip())
            x1 = float(line_array[1].lstrip().rstrip())
            y0 = float(line_array[2].lstrip().rstrip())
            y1 = float(line_array[3].lstrip().rstrip())
        if(line.find("output_file_prefix")!=-1):
            out_file = line.replace("output_file_prefix","").lstrip().rstrip()

    field_edges = numpy.zeros([4])

    size_x = x1 - x0
    size_y = y1 - y0
    nominal_x = size_x / num_x
    nominal_y = size_y / num_y
    x = nominal_x + overlap_x
    y = nominal_y + overlap_y 

    index = 0
    for i in range(0,int(num_y)):
        for j in range(0,int(num_x)):
            if(j==0):
                field_edges[0] = x0 
                field_edges[1] = field_edges[0]+x
            elif(j==num_x-1):
                field_edges[0] = x1 - x
                field_edges[1] = x1
            else:
                field_edges[0] = x0 + j*nominal_x - overlap_x
                field_edges[1] = x0 + (j+1)*nominal_x + overlap_x
            if(i==0):
                field_edges[2] = y0 
                field_edges[3] = y0+y
            elif(i==num_y-1):
                field_edges[2] = y1 - y
                field_edges[3] = y1
            else:
                field_edges[2] = y0 + i*nominal_y - overlap_y
                field_edges[3] = y0 + (i+1)*nominal_y + overlap_y

            current_out_conf = output_mask +"_"+str(i)+"_"+str(j)
            current_ascii_out = open(current_out_conf+".conf",'w')
            initial_config.seek(0)

            for line in initial_config:
                if(line.find("catalogue_window")!=-1):
                    current_ascii_out.write("catalogue_window    " +str(field_edges[0])+","+str(field_edges[1])+","+str(field_edges[2])+","+str(field_edges[3])+"\n")
                elif(line.find("output_file_prefix")!=-1):
                    current_ascii_out.write("output_file_prefix    " + out_file +"_"+str(i)+"_"+str(j)  +"\n") 
                else:
                    current_ascii_out.write(line)

            current_ascii_out.close()


            
