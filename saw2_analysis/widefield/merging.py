import numpy
import h5py
from saw2_analysis.io.hdf5 import get_all_data_from_saw2_hdf5
 

# This routine combines a number of SaWLens 2 reconstrucitons into a single field. The input is a list of filenames which point to a SaW2 reconstruction saved in h5 file format. Another vector with a collection of masks can be provided, the mask will be applied to each reconstruction before the merging. The output is a combined map containg x, y, psi, alpha1, alpha2, kappa, shear1 and shear2. Be aware that psi is not unique and may result in discontinuieites when maps are combined.  

def combine_maps(files, masks=None):

    out = numpy.empty([0,8])
    index=0
    for file in files:
        current_h5 = h5py.File(file)
        map = get_all_data_from_saw2_hdf5(current_h5)
        if(masks != None):
            if((masks[index][0] != masks[index][1])&(masks[index][2] != masks[index][3])):
                mask = (map[:,0]>=masks[index][0])&(map[:,0]<=masks[index][1])&(map[:,1]>=masks[index][2])&(map[:,1]<=masks[index][3])
                map = map[mask]
        
        out = numpy.concatenate([out,map])
        index += 1

    return out
# This routine creates standard mask files, assuming that the output fields are arranged from bottom-left to top-right

def create_standard_masks(x_tiles, y_tiles, x_size, y_size, x_corner, y_corner):

    output = numpy.empty([x_tiles*y_tiles,4])

    standard_x = [] 
    standard_y = [] 
    for i in range(0,x_tiles+1):
        standard_x.append(x_corner+i*x_size)
    for i in range(0,y_tiles+1):
        standard_y.append(y_corner+i*y_size)
    standard_x=numpy.array(standard_x)
    standard_y=numpy.array(standard_y)

    index = 0
    for i in range(0,y_tiles):
        for j in range(0,x_tiles):
            output[index][0] = standard_x[j]
            output[index][1] = standard_x[j+1]
            output[index][2] = standard_y[i]
            output[index][3] = standard_y[i+1]
            index += 1

    return output
