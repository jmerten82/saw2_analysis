import h5py
import numpy
import pyfits
import time
import matplotlib.pyplot as plt
from astropy.table import Table
from astropy.cosmology import FlatLambdaCDM
from saw2_analysis.visualisation.voronoi import voronoi_map


#The following class implements a collection of nodes, each with typical lensing properties attached to them, such as the potential, deflection angles, convergence, shear and redshift information. It can be constructed directly from a SaW2 HDF5 input file.   

class MeshFreeMap(object):

#Standard constructor,either creates a new instance from an HDF5 file or an empty object
    def __init__(self,file=None,inner=-1,outer=-1,name=None,mask=None):
        if(file!=None):
            h5file = h5py.File(file,'r')
            if(name == None):
                self.name = h5file.attrs["Name"]
            else:
                self.name = name

            scale = numpy.array((h5file.attrs["Coordinate scale"],h5file.attrs["Coordinate x-shift"],h5file.attrs["Coordinate y-shift"]))
            self.z = numpy.array((h5file.attrs["Lens redshift"],h5file.attrs["Fiducial redshift"]))
            path1 = "/"
            path2 = "/"
            if(outer != -1 and inner != -1):
                path1 += "iterations/outer" +str(outer) +"/coordinates"
                path2 = path1 + "/inner" +str(inner) +"/reconstruction" 
            else:
                path1 += "reconstruction/coordinates"
                path2 += "reconstruction/reconstruction"

            coords = numpy.array(h5file[path1])
            coords[:,0] += scale[1]
            coords[:,1] += scale[2]
            coords *= scale[0]
            my_mask = numpy.ones(len(coords),dtype=bool)
            if(mask!=None):
                if(len(mask) == 3):
                    my_mask = (coords[:,0]-mask[0])**2 + (coords[:,1]-mask[1])**2 < mask[2]**2  
                elif(len(mask) > 3):
                    my_mask = (coords[:,0] > mask[0]) & (coords[:,0] < mask[1]) & (coords[:,1] > mask[2]) & (coords[:,1] < mask[3])
            self.phys_coords = coords[my_mask] 
            self.psi = numpy.array(h5file[path2]["psi"][my_mask])
            self.alpha1 = numpy.array(h5file[path2]["alpha1"][my_mask])
            self.alpha2 = numpy.array(h5file[path2]["alpha2"][my_mask])
            self.kappa = numpy.array(h5file[path2]["kappa"][my_mask])
            self.shear1 = numpy.array(h5file[path2]["gamma1"][my_mask])
            self.shear2 = numpy.array(h5file[path2]["gamma2"][my_mask])

        else:
            self.phys_coords = numpy.zeros([0,2])
            self.psi = numpy.zeros(0)
            self.alpha1 = numpy.zeros(0)
            self.alpha2 = numpy.zeros(0)
            self.kappa = numpy.zeros(0)
            self.shear1 = numpy.zeros(0)
            self.shear2 = numpy.zeros(0)
            if(name==None):
                self.name = ""
            else:
                self.name = str(name)
            self.z = numpy.array((-1.,-1))

    #This returns the number of nodes in the system
    @property
    def num_nodes(self):
        return len(self.phys_coords)

    #This returns redshift information
    @property
    def z_lens(self):
        return self.z[0]
        
    @property
    def z_map(self):
        return self.z[1]



    #Lets you add nodes either in the form of a numpy data array of shape (num_new_nodes,8)
    def add(self, data=None, file=None,mask=None,cosmo=None,inner=-1,outer=-1):
        if(data != None):
            if(numpy.shape(data)[1] < 8):
                raise TypeError("MeshFreeMap: Not enough data to add.")
            self.phys_coords = numpy.vstack([self.phys_coords,zip(data[:,0],data[:,1])])
            self.psi = numpy.append(self.psi,data[:,2])
            self.alpha1 = numpy.append(self.alpha1,data[:,3])
            self.alpha2 = numpy.append(self.alpha2,data[:,4])
            self.kappa = numpy.append(self.kappa,data[:,5])
            self.shear1 = numpy.append(self.shear1,data[:,6])
            self.shear2 = numpy.append(self.shear2,data[:,7])
        elif(file != None):
            if(type(file) != numpy.ndarray):
                file = numpy.array([file])
            if(type(mask) != numpy.ndarray):
                mask = numpy.array([mask])
            if(type(inner) != numpy.ndarray):
                inner = numpy.array([inner])
            if(type(outer) != numpy.ndarray):
                outer = numpy.array([outer])

            for i in range(0,len(file)):
                h5file = h5py.File(file[i],'r')
                scale = numpy.array((h5file.attrs["Coordinate scale"],h5file.attrs["Coordinate x-shift"],h5file.attrs["Coordinate y-shift"]))
                if(self.name == ""):
                    self.name = h5file.attrs["Name"]
                if(self.z[0] == -1.):
                    self.z[0] = h5file.attrs["Lens redshift"]
                else:
                    if(self.z[0] != h5file.attrs["Lens redshift"]):
                        raise ValueError("MeshFreeMap: Lens redshift mismatch")
                scaling = 1.
                if(self.z[1] == -1.):
                    self.z[1] = h5file.attrs["Fiducial redshift"]
                else:
                    if(self.z[1] != h5file.attrs["Fiducial redshift"]):
                        if(cosmo == None):
                            cosmo = FlatLambdaCDM(H0=70, Om0=0.3)
                            scaling =  (cosmo.angular_diameter_distance_z1z2(self.z[0],self.z[1]) / cosmo.angular_diameter_distance_z1z2(0.,self.z[1]) * cosmo.angular_diameter_distance_z1z2(0.,h5file.attrs["Fiducial redshift"]) / cosmo.angular_diameter_distance_z1z2(self.z[0],h5file.attrs["Fiducial redshift"])).value
                try:
                    current_mask = mask[i]
                except IndexError:
                    current_mask = None
                try:
                    current_outer = outer[i]
                except IndexError:
                    current_outer = -1
                try:
                    current_inner = inner[i]
                except IndexError:
                    current_inner = -1


                path1 = "/"
                path2 = "/"
                if(current_outer != -1 and current_inner != -1):
                    path1 += "iterations/outer" +str(outer) +"/coordinates"
                    path2 = path1 + "/inner" +str(inner) +"/reconstruction" 
                else:
                    path1 += "reconstruction/coordinates"
                    path2 +=  "reconstruction/reconstruction"

                coords = numpy.array(h5file[path1])
                coords[:,0] += scale[1]
                coords[:,1] += scale[2]
                coords *= scale[0]
                my_mask = numpy.ones(len(coords),dtype=bool)
                if(current_mask!=None):
                    if(len(current_mask) == 3):
                        my_mask = (coords[:,0]-current_mask[0])**2 + (coords[:,1]-current_mask[1])**2 < current_mask[2]**2  
                    elif(len(current_mask) > 3):
                        my_mask = (coords[:,0] > current_mask[0]) & (coords[:,0] < current_mask[1]) & (coords[:,1] > current_mask[2]) & (coords[:,1] < current_mask[3])
                self.phys_coords = numpy.vstack([self.phys_coords,coords[my_mask]]) 
                self.psi = numpy.append(self.psi,h5file[path2]["psi"][my_mask])
                self.alpha1 = numpy.append(self.alpha1,h5file[path2]["alpha1"][my_mask])
                self.alpha2 = numpy.append(self.alpha2,h5file[path2]["alpha2"][my_mask])
                self.kappa = numpy.append(self.kappa,h5file[path2]["kappa"][my_mask])
                self.shear1 = numpy.append(self.shear1,h5file[path2]["gamma1"][my_mask])
                self.shear2 = numpy.append(self.shear2,h5file[path2]["gamma2"][my_mask])
                    
                    
    #Give the option of an array of files and masks here

    def set_redshifts(self,z_lens=-1.,z_map=-1.,rescale=True,cosmo=None):
        if(z_map == -1.):
            z_map = self.z[1]
        if(z_lens == -1.):
            z_lens = self.z[0]
        if(z_map < z_lens):
            raise ValueError("MeshFreeMap: Map must be behind lens.")
    
        scaling = 1.
        if(self.z[0] == -1. or self.z[0] == -1.):
            scaling = 1.
        else:
            if(cosmo == None):
                cosmo = FlatLambdaCDM(H0=70, Om0=0.3)
                scaling =  (cosmo.angular_diameter_distance_z1z2(z_lens,z_map) / cosmo.angular_diameter_distance_z1z2(0.,z_map) * cosmo.angular_diameter_distance_z1z2(0.,self.z[1]) / cosmo.angular_diameter_distance_z1z2(self.z[0],self.z[1])).value

        self.z[0] = z_lens
        self.z[1] = z_map
        self.psi *= scaling
        self.alpha1 *= scaling
        self.alpha2 *= scaling
        self.kappa *= scaling
        self.shear1 *= scaling
        self.shear2 *= scaling

    #This returns an astropy Table
    
    def table(self):
        mytable = Table([self.phys_coords[:,0],self.phys_coords[:,1],self.psi,self.alpha1,self.alpha2,self.kappa,self.shear1,self.shear2],names=('x','y','psi','alpha1','alpha2','kappa','gamma1','gamma2'),dtype=('f8','f8','f8','f8','f8','f8','f8','f8'))
        return mytable

    def return_data(self,selection="all"):
        if(selection == "coords"):
            return self.phys_coords
        elif(selection == "x"):
            return self.phys_coords[:,0]
        elif(selection == "y"):
            return self.phys_coords[:,1]
        elif(selection == "psi"):
            return self.psi
        elif(selection == "alpha1"):
            return self.alpha1
        elif(selection == "alpha2"):
            return self.alpha2
        elif(selection == "kappa"):
            return self.kappa
        elif(selection == "shear1"):
            return self.shear1
        elif(selection == "shear2"):
            return self.shear2
            
    def write(self,file,format="ascii"):
        if(format=="hdf5"):
            this_h5_file = h5py.File(file,'a')
            this_h5_file.attrs["Lens redshift"] = self.z[0]
            this_h5_file.attrs["Fiducial redshift"] = self.z[1]
            this_h5_file.attrs["Name"] = self.name
            this_h5_file.attrs["Creator"] = "saw2analysis.py"
            this_h5_file.attrs["Time (UTC)"] = time.strftime("%a %b %d %H:%M:%S %Y",time.gmtime()) 
            this_h5_file.close()
            
            mytable = self.table()
            mytable.write(file,format="hdf5",path="reconstruction/reconstruction",append=True,overwrite=True)


        elif(format=="fits"):
            x_col = pyfits.Column(name='x',format='D',array=self.phys_coords[:,0])
            y_col = pyfits.Column(name='y',format='D',array=self.phys_coords[:,1])
            ct_col = pyfits.Column(name='psi',format='D',array=self.psi) 
            cp_col = pyfits.Column(name='alpha1',format='D',array=self.alpha1) 
            s1t_col = pyfits.Column(name='alpha2',format='D',array=self.alpha2) 
            s1p_col = pyfits.Column(name='kappa',format='D',array=self.kappa)
            s2t_col = pyfits.Column(name='gamma1',format='D',array=self.shear1) 
            s2p_col = pyfits.Column(name='gamma2',format='D',array=self.shear2)
            tbhdu = pyfits.new_table([x_col,y_col,ct_col,cp_col,s1t_col,s1p_col,s2t_col,s2p_col])
            tbhdu.header["ZLENS"] = self.z[0]
            tbhdu.header["ZMAP"] = self.z[1]
            tbhdu.writeto(file,clobber=True)
                
        else:
            output = open(file,'w')
            print >>output, "# zlens = " + str(self.z[0]) + " z_map = " + str(self.z[1])
            
            for i in range(0,len(self.phys_coords)):
                print >>output, self.phys_coords[i][0], self.phys_coords[i][1], self.psi[i], self.alpha1[i], self.alpha2[i], self.kappa[i], self.shear1[i], self.shear2[i]
                
            output.close()


    #This just gives a quick representation of the node distribution. More sophisticated visualisation routine are in the visulaisation package. The mask option is used if you want to restrict the output to a certain area and assumes a list with x_min, x_max, y_min, y_max

    def visualise(self,mask=None):
        self.fig, self.ax = plt.subplots()
        self.ax.set_xlabel("x")
        self.ax.set_ylabel("y")
        self.ax.set_title(self.name)
        if(type(mask)==list):
            if(len(mask) > 3):
                self.ax.set_xlim([mask[0],mask[1]])
                self.ax.set_ylim([mask[2],mask[3]])
        self.ax.plot(self.phys_coords[:,0],self.phys_coords[:,1],'ko')

    #This returns an eventually oversampled Voronoi tesselation of the mesh free map and outputs it as a 2D array. You can choose the number of pixels on the x axis, together with an output window. If the output window is empty, the min and max coordinate values on each axes are calculated first. If the mask threshold is set to something postive, the routine checks if the distance of each oversampled pixel to its nearest real number is larger than the threshold. If so, the pixel is set to the mask_value value.  

    def return_map(self,x_sampling=1024,x_min=None,x_max=None,y_min=None,y_max=None, mask_threshold = -1, mask_value = 0.):

        map = numpy.zeros([len(self.kappa),3])
        for i in range(0,len(self.kappa)):
            map[i][0] = self.phys_coords[i][0]
            map[i][1] = self.phys_coords[i][1]
            map[i][2] = self.kappa[i]

        return voronoi_map(map,x_sampling,x_min,x_max,y_min,y_max, mask_threshold, mask_value)


        
        
