import scipy
import numpy
from scipy import spatial

# This creates a radial profile from the input binning scheme and a MeshFreeMap. It returns the bin centre based on the sample median, the average map value in bin, the std in the bin and the number of objects as a structure with shape (numbins,4)

def create_profile(bins, data, x_ref = 0., y_ref = 0.):
    radii = []

    for node in data.phys_coords:
        r = numpy.sqrt((node[0]-x_ref)**2 + (node[1]-y_ref)**2)
        radii.append(r)

    num_bin = len(bins)-1
    bin_output=numpy.zeros([num_bin,4])


    for bin in range(0,num_bin):
        kappa_samples=[]
        radius_samples=[]
        for i in range(0,len(radii)):
            radius = radii[i]
            if(radius > bins[bin] and radius <= bins[bin+1]):
                radius_samples.append(radius)
                kappa_samples.append(data.kappa[i])

        radius_samples=numpy.array(radius_samples)
        kappa_samples=numpy.array(kappa_samples)
        bin_output[bin][0] = numpy.mean(kappa_samples)
        bin_output[bin][1] = numpy.median(radius_samples)
        bin_output[bin][2] = numpy.std(kappa_samples)
        bin_output[bin][3] = len(kappa_samples)

    return bin_output


#This routine takes an array of MeshFreeMap BS realisations, bins the map into the bins selected by the binning scheme and calculates for each bin mean, std and covariance matrix according to all realisations. The routine retunrs an array of the bin means, stds and the covariance matrix. 

def BS_profile_analysis(bins,BS_map_array):

    numBS = len(BS_map_array)
    numBins = len(bins)-1

    profile_container = []
    for current in BS_map_array:
        profile_container.append(create_profile(bins, current))
    profile_container = numpy.array(profile_container)

    bin_samples = numpy.zeros([numBins,numBS])
    for bin in range(0,numBins):
        for BS in range(0,numBS):
            bin_samples[bin][BS] = profile_container[BS][bin][0]

    means = numpy.zeros(numBins)
    stds = numpy.zeros(numBins)
    for bin in range(0,numBins):
        means[bin] = numpy.mean(bin_samples[bin])
        stds[bin] = numpy.std(bin_samples[bin])

    cov = numpy.cov(bin_samples)

    return means, stds, cov
        



#This routine degrades a regular map to an arbitrarily scattered collection of points by finding for each pixel of the map the nearest neighbour of the node collection and averaging them all. Output is a map with entiries of form x, y and averaged value. x and y are here the coordinates of the scattered node collection.A coordinate reference for the bottom left point of the regular map must be given, together with the pixel scale. The regular map is assumed to be in a 2D array form.   

def degrade_to_scattered_nodes(regular_map, scattered_map, x_ref, y_ref, pixel_scale):

    num_nodes = len(scattered_map)
    coords = scattered_map[:,0:2]
    Tree=scipy.spatial.cKDTree(coords,leafsize=128)

    y_dim = numpy.shape(regular_map)[0]
    x_dim = numpy.shape(regular_map)[1]
    in_nodes  = x_dim*y_dim

    samples = numpy.zeros([num_nodes])
    numbers = numpy.zeros([num_nodes])

    for i in range(0,y_dim):
        for j in range(0,x_dim):
            x = x_ref + j*pixel_scale
            y = y_ref + i*pixel_scale
            map=Tree.query((x,y),1)
            samples[map[1]] += regular_map[i][j]
            numbers[map[1]] += 1

    new_values = samples/numbers
    output = numpy.zeros([num_nodes,3])
    for i in range(0,num_nodes):
        output[i][0] = scattered_map[i][0]
        output[i][1] = scattered_map[i][1]
        output[i][2] = new_values[i]

    return output

#This routine averages an existing map within square boxes of size box

def box_average(map,box=4):
    size_x = numpy.shape(map)[1]/box
    size_y = numpy.shape(map)[0]/box

    output = numpy.zeros([size_y,size_x])

    for i in range(0,size_y):
        for j in range(0,size_x):
            sample = numpy.zeros(box*box)
            x_start = j*box
            y_start = i*box
            index = 0
            for l in range(0,box):
                for k in range(0,box):
                    sample[index] = map[y_start+l][x_start+k]
                    index += 1
            output[i][j] = numpy.mean(sample)

    return output
            
            


        
    
    


            
