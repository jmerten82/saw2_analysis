import scipy
import numpy
from scipy import spatial

#This routine calculates the critical lines in a lensing map. It expects data in the form of a MeshFreeMap instance. The second argument defines the number of nearest neighbours to be search for jacdet sign changes.

def find_critical_curve(data, nn = 4):
    jacdet = (1.-data.kappa)**2.-data.shear1**2.-data.shear2**2.
    Tree=scipy.spatial.cKDTree(data.phys_coords,leafsize=128)
    search_nn = nn+1
    crit_line_set=[]
    for element in data.phys_coords:
        map=Tree.query(element,search_nn)
        for neighbour in range(1,search_nn):
            if(jacdet[map[1][0]]*jacdet[map[1][neighbour]] < 0.):
                current_x = (data.phys_coords[map[1][0]][0] + data.phys_coords[map[1][neighbour]][0]) / 2. 
                current_y = (data.phys_coords[map[1][0]][1] + data.phys_coords[map[1][neighbour]][1]) / 2.
                crit_line_set.append(numpy.array((current_x,current_y)))

    crit_line_set=numpy.array(crit_line_set)
    return crit_line_set

#This creates a large set of points, all of which derive from a critical curve analsysis of a full set of bootstraps. The input here is an array of jacdet maps. The output is a an array of coorindates. 0: x-coordinate, 1:y.  Also here you can choose the number of nn for the ccruve search.

def BS_critical_curve_analysis(BS_array, numNN = 4):

    points = numpy.empty([0,2])
    
    for current in BS_array:
        current_set = find_critical_curve(current, numNN)
        points = numpy.concatenate((points,current_set),0)

    return points

#This calculates the Einstein radius from a MeshFreeMap. The centre can be defined, together with a minimal radius in order to exclude the radial critical line. Again, you can also choose the number of nearest neighbours to find the critical points. It retuns the einstein radius and its std from azimuthal averaging.

def calculate_einstein_radius(data,x_ref = 0.,y_ref = 0.,thresh=0.,nn=4):

    crit_points =  find_critical_curve(data,nn)
    radius_sample = []
    for current in crit_points:
        dist = numpy.sqrt((current[0]-x_ref)**2.+(current[1]-y_ref)**2.)
        if(dist > thresh):
            radius_sample.append(dist)
    radius_sample = numpy.array(radius_sample)
    return numpy.mean(radius_sample), numpy.std(radius_sample)

#This is the same routine but works on an array of MeshFreeMaps and returns the average Einstein radius with a std based on the sample variance.

def calculate_einstein_radius_from_BS(data,x_ref = 0.,y_ref = 0.,thresh=0.,nn=4):
    re_sample = []
    for current in data:
        mean, std = calculate_einstein_radius(current,x_ref,y_ref,thresh,nn)
        re_sample.append(mean)

    re_sample = numpy.array(re_sample)
    return numpy.mean(re_sample), numpy.std(re_sample)

