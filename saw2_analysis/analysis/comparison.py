import scipy
import numpy
from scipy import spatial


#This compares two maps. Map is defined as a collection of three arrays, which contain the two coordinates and the map values. Matches are found via a kd-tree matching of single values. Output is again a map with coordinates, the absolute difference of map - reference, the relative difference (map-reference)/reference and the abolsute value of the relative difference |map-reference|/reference. If knn is set, the knn nearest neighbours in the reference are averaged.

def compare_maps(map,reference,knn=1):

    ref_coords = reference[:,0:2]
    Tree=scipy.spatial.cKDTree(ref_coords,leafsize=128)
    output = numpy.zeros([len(map),5])
    index = 0

    for node in map:
        node_coord = node[0:2]
        node_value = node[2]
        map=Tree.query(node_coord,knn)
        output[index][0] = node_coord[0]
        output[index][1] = node_coord[1]

        if(knn > 1):
            value_sample = []
            for index in range(0,knn):
                value_sample.append(reference[map[1][index]][2])
            value_sample = numpy.array(value_sample)
            reference_value = numpy.mean(value_sample)
        else:
            reference_value = reference[map[1]][2]

        diff = node_value - reference_value
        output[index][2] = diff
        output[index][3] = diff/reference_value
        output[index][4] = numpy.abs(diff/reference_value)

        index += 1

    return output

#This is many maps version of the routine above. Here the routine takes an array of input maps, compares first each of them to the reference, and then assigns the outcome to the closest coordinates in the coordinate_reference. This is because the maps in the array could have slightly differing coordinates. The output is a map with the means of absolute, relative and absolute relative difference and the respeactive stds. 
 
def compare_maps_BS(map_array,reference, coordinate_reference, knn=1):
    
    output = numpy.zeros([len(coordinate_reference),8])
    index = 0

    abs_sample = numpy.zeros([len(coordinate_reference),len(map_array)])
    rel_sample = numpy.zeros([len(coordinate_reference),len(map_array)])
    absrel_sample = numpy.zeros([len(coordinate_reference),len(map_array)])
    for current in map_array:
        current_out = compare_maps(current,reference,knn)
        Tree=scipy.spatial.cKDTree(current_out[:,0:2],leafsize=128)
        for index2 in range(0,len(coordinate_reference)):
            map = Tree.query((coordinate_reference[index2][0],coordinate_reference[index2][1]),1)
            abs_sample[index2][index] = current_out[map[1]][2]
            rel_sample[index2][index] = current_out[map[1]][3]
            absrel_sample[index2][index] = current_out[map[1]][4]
        index += 1

    for final in range(0,len(coordinate_reference)):
        output[final][0] = coordinate_reference[final][0]
        output[final][1] = coordinate_reference[final][1]
        output[final][2] = numpy.mean(abs_sample[final])
        output[final][3] = numpy.std(abs_sample[final])
        output[final][4] = numpy.mean(rel_sample[final])
        output[final][5] = numpy.std(rel_sample[final])
        output[final][6] = numpy.mean(absrel_sample[final])
        output[final][7] = numpy.std(absrel_sample[final])

    return output
